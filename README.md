# Aulas Tópicos Especiais I - 2021/2

## 1. Introdução e Apresentação da Disciplina

### 1.1 Apresentação da disciplina

- Objetivo;
- Metodologia;
- Bibliografia;
- Registro de participação;
- Avaliação.

### 1.2  Ferramentas

- GIT;
- VS Code;
- NodeJS;
- Ionic 4;
- BitBucket.

### 1.3 Configurações

- Instalar GIT e NodeJS;
- Instalar o VS Code;
- Instalar Ionic;
- Criar primeiro projeto;
- Criar repositório;
- Conectar o projeto ao repositório.

### 1.4 Versionamento

- Executando o código inicial;
- Criação do repositório no Bitbucket;
- Conexão do código local com remoto;
- Primeiro commit.

## Hands On - 01

Ao término desta aula é esperado que você aluno(a) tenha realizado o seguinte:

    -> Instalação das ferramentas necessárias à realização das atividades da disciplina;
    -> Configuração do Ionic e criação do projeto controle;
    -> Início do versionamento do código;
    -> Envio do link para o repositório do BitBucket.

## 2. Interface Gráfica

### 2.1 Criação de páginas

- Estrutura da aplicação;
- Compilação incremental;
- Introdução às tags do Ionic;
- Geração de páginas.

### 2.2 Estilo CSS

- Navegação;
- Mais _TAGs_ HTML5;
- Definição de estilos;
- Inserção de imagem;
- Primeiros _inputs_;
- _Padding_;
- Cores pré-definidas;
- Cores personalizadas.

### 2.3 Criando o model

- _Data binding_;
- Criação de variáveis;
- Criação de métodos;
- injeção de dependência;
- Navegação programática;
- Exibição de **_Toast_**.

## Hands On - 02

Ao término desta aula é esperado que você aluno(a) tenha realizado o seguinte:

    -> Tenha criado a página de login;
    -> Tenha criado seu estilo para o seu aplicativo;
    -> Tenha definido seu próprio logotipo para o iFinanças;
    -> Saiba criar variáveis e métodos usando Typescript;
    -> Saiba fazer navegação programática e exibição de mensagem de erro.

## 3. Reutilização de Código

### 3.1. Formulários e Validação

- Forms no _Ionic_;
- _ReactiveForm_;
- _FormGroup_ e _FormControl_;
- _FormBuilder_;
- Validação de entrada;
- _Binding_ de método para _submit_;
- Obtendo valor do _FormGroup_.

### 3.2. Organização do código

- Separação de responsabilidades;
- Lógica de apresentação;
- Uso de serviços;
- Delegação;
- Refatoração.

### 3.3. Componentes e Módulos

- Mais reutilização de código;
- Criação de componentes;
- Criação de módulos;
- Declaração, importação e exportação do componente;
- Criação de novas _tags_ **HTML5**;
- Reutilização do estilo.

## 4. Reutilização de Código.

### 4.1. Concluindo o controle de acesso.

- Reutilização de código pelo uso de componentes;
- Introdução ao uso do _Firebase_;
- Criação do primeiro projeto;
- Controles do _Firebase_;
- Configuração da página inicial.

### 4.2. Navegação.

- Criação de rodapé;
- Navegação com _RouterLink_;
- _Single Page Application_ - *SPA*;
- Sistema de grade do _Ionic_.

### 4.3. Configurando o *Firebase* no projeto.

- Envio de argumentos para componentes;
- Uso de _inputs_ em componentes;
- Mais sobre o uso de serviços;
- Delegação;
- Ativando autenticação por _e-mail_;
- Criação do _app_ no _Firebase_;
- _FirebaseConfig_;
- Variáveis de ambiente do _Ionic_.

## 5. Ciclo de vida do controle de acesso.

### 5.1. Configuração do Firebase Auth.

- Instalação e configuração do *Angular Fire*;
- Conexão da aplicação com o *Firebase*;
- Uso do *FireAuth*;
- Criação de usuários no *Firebase*;
- Credenciais do usuário;
- Visualização de usuários no *Firebase*.

### 5.2. **Login** e **logout**.

- _Login_ do usuário;
- Redirecionamento após _login_;
- Exibição da mensagem de erro;
- Verificação de usuário logado;
- _Login_ automático;
- Noção de uso de _Observable_;
- _Logout_;
- Uso de _promises_.

### 5.3. Recuperação de senha.

- _Reset_ de senha;
- _Templates_ do *Firebase Auth*;
- Configuração da mensagem de _reset_.

## 6. Modularização de Aplicação.

### 6.1. Por quê modularizar?

- Modularização de aplicações;
- Mais reutilização de código;
- Organização dos caminhos do módulo;
- Geração automática de módulos;
- Parâmetros de criação: _flat_ e routing;
- Módulo roteado;
- Rotas filhas;
- Carregando páginas como componentes;
- Declaração de páginas como componentes;
- _Imports_ necessários.

### 6.2. Como modularizar?

- Redução de dependências;
- Componentes compartilhados **vs** internos;
- Organização de serviços;
- Ajuste de caminhos;
- Comunicação entre componentes;
- Criando um módulo do _zero_.

## 7. CRUD de contas.

### 7.1. Revendo _forms_.

- Criação de _forms_;
- Uso de serviços.

### 7.2. Banco de dados de tempo real.

- Criando o banco de dados;
- Acessando o **Firestore**;
- CRUD de contas.

## 8. Conclusão do CRUD.

### 8.1. Edição de dados de um objeto.

- Uso do **AlertController**;
- _Alert_ com formulário.

### 8.2. Generalização das listas de contas.

- ___BackButton___;
- _Header_ variável;
- Mais sobre botões.

## 9. Revisão e Aprofundamento sobre Componentes.

## 10. Helpers, Pipes e Seletor de Datas.

## 11. Teste de Aplicação.

### 11.1. Noções iniciais sobre teste.

- Princípios básicos;
- Custo do teste;
- Estrutura dos testes.

### 11.2. Teste de Unidade.

- Testando serviços;
- Testando componentes.

### 12. Teste de Funcionalidade.
