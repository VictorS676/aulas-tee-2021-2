
export class DateHelper{
     
   /**
   * Quebra date em dia, mês e ano.
   * 
   * @param date: string - data no formato ISO.
   * @returns object: Ano, Mês e Dia
   */
  static breakDate(date){
    const aux = (date.split('T')[0]).split('-');
    return {ano: parseInt(aux[0], 10), mes: parseInt(aux[1], 10), dia: parseInt(aux[2], 10)};
  }
}