import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { By } from '@angular/platform-browser';
import { IonicModule } from '@ionic/angular';

import { CalculadoraPage } from './calculadora.page';

describe('A página Calculadora', () => {
  let view;
  let model: CalculadoraPage;
  let fixture: ComponentFixture<CalculadoraPage>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ CalculadoraPage ],
      imports: [IonicModule.forRoot(), FormsModule, ReactiveFormsModule]
    }).compileComponents();

    fixture = TestBed.createComponent(CalculadoraPage);
    model = fixture.componentInstance;
    view = fixture.nativeElement;
    fixture.detectChanges();
  }));

  it('deve ter um título', () => {
    expect(model.titulo).toBeDefined();
  });

  it('deve renderizar o título', () => {
    const result = view.querySelector('.title').textContent;
    expect(result).toEqual('Calculadora');
  });

  it('deve estar com o botão desabilitado', () => {
    expect(model.form.invalid).toBeTrue();
  });

  it('divide com clique no botão', () => {
    model.form.controls.valor1.setValue(4);
    model.form.controls.valor2.setValue(20);
    model.form.controls.operacao.setValue('divisao');

    const calculo = fixture.debugElement.query(By.css('#calculo'));
    calculo.triggerEventHandler('click', null);
    fixture.detectChanges();

    const resultado = view.querySelector('#resultado');
    expect(resultado.textContent).toEqual('5');
  });

  it('multiplica com clique no botão', () => {
    model.form.controls.valor1.setValue(12);
    model.form.controls.valor2.setValue(3);
    model.form.controls.operacao.setValue('multiplicacao');

    const calculo = fixture.debugElement.query(By.css('#calculo'));
    calculo.triggerEventHandler('click', null);
    fixture.detectChanges();

    const resultado = view.querySelector('#resultado');
    expect(resultado.textContent).toEqual('36');
  });

  it('adiciona com clique no botão', () => {
    model.form.controls.valor1.setValue(159);
    model.form.controls.valor2.setValue(786);
    model.form.controls.operacao.setValue('adicao');

    const calculo = fixture.debugElement.query(By.css('#calculo'));
    calculo.triggerEventHandler('click', null);
    fixture.detectChanges();

    const resultado = view.querySelector('#resultado');
    expect(resultado.textContent).toEqual('945');
  });

  it('subtraí com clique no botão', () => {
    model.form.controls.valor1.setValue(7);
    model.form.controls.valor2.setValue(5);
    model.form.controls.operacao.setValue('subtracao');

    const calculo = fixture.debugElement.query(By.css('#calculo'));
    calculo.triggerEventHandler('click', null);
    fixture.detectChanges();

    const resultado = view.querySelector('#resultado');
    expect(resultado.textContent).toEqual('2');
  });
});
