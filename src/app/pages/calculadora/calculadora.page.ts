import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { CalculadoraService } from 'src/app/services/calculadora.service';

@Component({
  selector: 'calculadora',
  templateUrl: './calculadora.page.html',
  styleUrls: ['./calculadora.page.scss'],
})
export class CalculadoraPage implements OnInit {
  titulo = 'Calculadora';
  form: FormGroup;
  resultado: string | number;

  constructor(
    private builder: FormBuilder,
    private calculadora: CalculadoraService
  ){}

  ngOnInit(){
    this.form = this.builder.group({
      valor1: ['', [Validators.required]],
      valor2: ['', [Validators.required]],
      operacao: ['', [Validators.required]]
    });
  }

  calcular(){
    const data = this.form.value;
    const operacao = data.operacao;
    switch(operacao){
      case 'adicao':
        this.somar();
        break;
      case 'subtracao':
        this.subtrair();
        break;
      case 'multiplicacao':
        this.multiplicar();
        break;
      case 'divisao':
        this.dividir();
        break;
      default:
        this.resultado = 'Operação Inválida';
        break;
    }
  }

  somar(){
    const data = this.form.value;
    const parcela1 = data.valor1;
    const parcela2 = data.valor2;
    this.resultado = this.calculadora.soma(parcela1, parcela2);
  }

  subtrair(){
    const data = this.form.value;
    const minuendo = data.valor1;
    const subtraendo = data.valor2;
    this.resultado = this.calculadora.subtrai(minuendo, subtraendo);
  }

  multiplicar(){
    const data = this.form.value;
    const multiplicando = data.valor1;
    const multiplicador = data.valor2;
    this.resultado = this.calculadora.multiplica(multiplicando, multiplicador);
  }

  dividir(){
    const data = this.form.value;
    const divisor = data.valor1;
    const dividendo = data.valor2;
    this.resultado = this.calculadora.divide(dividendo, divisor);
  }
}
