import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class CalculadoraService{

  constructor(){}

  soma(parcela1, parcela2){
    if(parcela1 < 0 || parcela2 < 0){
      return 'Os valores não podem ser negativos';
    }
    if(!(parcela1 % 1 === 0) || !(parcela2 % 1 === 0)){
      return 'Os valores devem ser inteiros';
    }
    return parcela1 + parcela2;
  }

  subtrai(minuendo, subtraendo){
    if(minuendo < 0 || subtraendo < 0){
      return 'Os valores não podem ser negativos';
    }
    if(minuendo < subtraendo){
      return 'O subtraendo não deve ser maior que o minuendo';
    }
    return minuendo - subtraendo;
  }

  multiplica(multiplicando, multiplicador){
    if(multiplicando < 0 || multiplicador < 0){
      return 'Os valores não podem ser negativos';
    }
    return multiplicando * multiplicador;
  }

  divide(dividendo, divisor){
    if(divisor === 0){
      return 'Não existe divisão por 0';
    }
    if(dividendo < 0 || divisor < 0){
      return 'Os valores não podem ser negativos';
    }
    return dividendo / divisor;
  }
}
