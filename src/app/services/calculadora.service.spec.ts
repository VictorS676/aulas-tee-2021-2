import { TestBed } from '@angular/core/testing';

import { CalculadoraService } from './calculadora.service';

describe('A classe Calculadora', () => {
  let calculadora: CalculadoraService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    calculadora = TestBed.inject(CalculadoraService);
  });

  describe('deve realizar divisões', () => {
    it('entre números inteiros', () => {
      const result = calculadora.divide(8, 4);
      expect(result).toBe(2);
    });

    it('com exceção do zero', () => {
      const result = calculadora.divide(8, 0);
      expect(typeof result).toEqual('string');
    });

    it('entre números positivos', () => {
      const result = calculadora.divide(52, -145);
      expect(typeof result).toEqual('string');
    });
  });

  describe('deve realizar multiplicação', () => {
    it('entre números inteiros', () => {
      const result = calculadora.divide(8, 4);
      expect(result).toBe(2);
    });

    it('entre números positivos', () => {
      const result = calculadora.divide(8, 4);
      expect(result).toBe(2);
    });
  });

  describe('deve realizar adição', () => {
    it('entre números inteiros', () => {
      const result = calculadora.divide(8, 4);
      expect(result).toBe(2);
    });

    it('entre números positivos', () => {
      const result = calculadora.divide(8, 4);
      expect(result).toBe(2);
    });
  });

  describe('deve realizar subtração', () => {
    it('entre números inteiros', () => {
      const result = calculadora.subtrai(10, 1);
      expect(result).toBe(9);
    });

    it('entre número positivos', () => {
      const result = calculadora.subtrai(22, -19);
      expect(typeof result).toEqual('string');
    });

    it('com o subtraendo menor que o dividendo', () => {
      const result = calculadora.subtrai(10, 16);
      expect(typeof result).toEqual('string');
    });
  });
});
